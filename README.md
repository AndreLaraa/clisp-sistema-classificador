# clisp-sistema-classificador

Questão 1 (Valor 7.0 pontos): Implemente uma função que:
- recebe um nome de classificador e o nome de um arquivo de regras
- leia o arquivo de regras e crie uma função com o nome clssificador especificado, que serve para classificar um exemplo de teste.

Exemplo:
> (cria-classificador 'diabetes "c:\\temp\\diabetes.txt")
Classificador criado com sucesso. Para executálo, avalie a expressão (diabetes...).

> (diabetes 6 148 72 35 0 33.6 0.627 50 )
tested_positive


 Questão 2 (Valor 3.0 pontos): implemente uma função que:
- dado um arquivo CSV com exemplos de teste, aplique o classificador criado na questão 1, chamando a função correspondente, em cada exemplo, gerando um arquivo de saída com os exemplos classificados.
Exemplo:
(avalia-geral 'diabetes "regraDiabetes.txt" "dadosDiabetes.csv" "../resultadoDiabetes.csv")