(defun parser-regra(regra)
	;regra = ((A > B) AND (B >= C)) 
	(let ((binario '()) (paraCond nil) )
		(dolist (elem regra)
				;cada lista é uma regra, ignora o AND
			 	(when (listp elem)
			 		;coloca em ordem (a > b) => (> a b)
					(setq binario (list (cadr elem) (car elem) (caddr elem)))
					(setq paraCond (append paraCond (list binario)))
				)
			)
			;(and (> a b) (>= c d))
			(setq paraCond	(cons 'AND paraCond))
		)
)	

(defun trata-arquivo-regras(classificador filename &optional flagClassificador)

	(with-open-file (stream filename)
		(let ((indiceLinha 1) (listaLinha nil) (regraLinha) (regraCond '() ) (retornoCond nil) )

			(loop for line = (read-line stream nil nil)
					while line do (progn ;percorre cada linha do arquivo de regras
						(setq listaLinha nil)
						(setq regraLinha '())
						; 
						(setq retornoCond nil)

						;pega a linha que é string e transforma ela em lista
						(with-input-from-string (s line)
						      (loop for part = (read s nil nil)
						            while part do (setq listaLinha (cons part listaLinha))
						            )
						      )
						;indice é 1 quando for a linha dos argumentos
						(when (= indiceLinha 1)
							;Pega parametros
							(setq parametros (cdr listaLinha))
							(setq parametros (reverse parametros))
							
							;ja tem parametros
							;agora percorre outras linhas separando regras e retorno
						)
						(unless (= indiceLinha 1)
							(setq listaLinha (reverse listaLinha))
							;cada elemento da linha é uma regra ou é o retorno da regra
							(dolist (elem listaLinha)
								;junta as regras em listas do tipo (a > b) (c >= d)
								(setq regraLinha (append regraLinha (list elem)))
									
									(when (equal (write-to-string elem) "=>")
										;identifica fim das regras e o retorno
										(setq regraLinha (delete '=> regraLinha))
										(setq retorno (last listaLinha))

										;pega o retorno sem a variável class
										(with-input-from-string (s (write-to-string retorno) :index i :start 7)
              								(setq retorno `((list ',(read s))) )
              								)

										;pega as regras faz o parser delas
										(setq regraLinha  (parser-regra regraLinha) )

										;quando a linha não possui regra é o default do COND
										(when (= (length regraLinha) 1)
											(setq regraLinha 'T)
											)
										;(> a b) (>= c d) => (and (> a b) (>= c d))
										(setq regraLinha (list regraLinha) )

										;(and (> a b) (>= c d)) => ((and (> a b) (>= c d)) ret)
										(setq regraCond (cons `(,@regraLinha ,@retorno) regraCond ))
										)
								)

							)
						(setq indiceLinha (+ indiceLinha 1))
					)
				)
				;((and (> a b) (>= c d)) ret) => (cond ( (and (> a b) (>= c d)) ret) )
				(setq regraCond (cons 'COND (reverse regraCond)))
				;(defun diabetes (a b c d) (cond ( (and (> a b) (>= c d)) ret) ) )
				(eval (list 'defun `(,@classificador) `(,@parametros) `(,@regraCond)))
				(unless flagClassificador
					;Aparece o texto só quando a funcao é chamada fora de outra
    				(format t "~%Classificador criado com sucesso. Para executálo, avalie a expressão (~d ~d)~%" classificador parametros)
					)
			)			
		)
	)
;(trata-arquivo-regras 'diabetes "regraDiabetes.txt")

;(avalia-geral 'diabetes "regraDiabetes.txt" "dadosDiabetes.csv" "../resultadoDiabetes.csv")
(defun avalia-geral (classificador regra origem destino )
	;cria classificador
	(trata-arquivo-regras classificador regra 1)
	(let ((line nil) (linhaDados))
		;abre arquivo de leitura
		(with-open-file (linhaLeitura origem)
			;abre arquivo de escrita, senao cria!
			(with-open-file (linhaEscrita destino :direction :output :if-exists :supersede :if-does-not-exist :create)
				(loop
					(setq line (read-line linhaLeitura nil 'eof))
					;fim do arquivo
					(when (eq line 'eof)
						(return)
						)
					;retira as vírgulas para passar como parametro
					(setq linhaDados (read-from-string (format nil "(~A)" (substitute #\SPACE #\, line)) nil 'eol))
					;escreve no arquivo 
					(format linhaEscrita "~a~a~%" (format nil "~{~A, ~}" linhaDados) (apply classificador linhaDados))


					)
				)
			)
		)
		t
	)